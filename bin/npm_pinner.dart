import 'dart:convert';
import 'dart:io';

import 'package:args/args.dart' as args;
import 'package:path/path.dart' as path;

Future<void> main(List<String> arguments) async {
  try {
    args.ArgParser parser = args.ArgParser()
      ..addOption('project', abbr: 'p')
      ..addFlag('describe', abbr: 'd', negatable: false)
      ..addFlag('write', negatable: false);
    args.ArgResults argResults = parser.parse(arguments);

    Directory targetWorkingDirectory = await getWorkingDirectory(argResults['project']);
    if (!targetWorkingDirectory.existsSync()) {
      throw 'the provided path does not exist: "${targetWorkingDirectory.path}"';
    }

    Map<String, dynamic> maybePackageJson = await parseProjectPackageJson(targetWorkingDirectory);
    Map<String, dynamic> maybePackageLockJson = await parseProjectPackageLockJson(targetWorkingDirectory);
    Map<String, dynamic> maybeNpmrc = await parseProjectNpmrc(targetWorkingDirectory);

    NodeProject project = NodeProject(targetWorkingDirectory, maybePackageJson, maybePackageLockJson, maybeNpmrc);

    if (argResults.wasParsed('describe')) {
      // ! this is effectively "debug" output, at least for the moment... it may become more meaningful later.
      project.describe();
    }

    if (argResults.wasParsed('write')) {
      // ! this triggers the actual update process, updating the relevant files with the intended changes.
    }
  } catch (error) {
    print('[fatal] $error');
    exit(1);
  }

  print('[debug] yay!!?! ..all done!');
}

class NodeProject {
  Directory directory;
  Map<String, dynamic> packageJson;
  Map<String, dynamic> packageLockJson;
  Map<String, dynamic> npmrc;

  // default class constructor
  NodeProject(
    this.directory,
    this.packageJson,
    this.packageLockJson,
    this.npmrc,
  );

  /// print out a human friendly summary of the project
  void describe() {
    print('-- DESCRIPTION START --');
    print('working directory:');
    print('  $directory');
    print('"package.json" (${packageJson.isNotEmpty ? 'valid' : 'invalid'}):');
    print('  $packageJson');
    print('"package-lock.json" (${packageLockJson.isNotEmpty ? 'valid' : 'invalid'}):');
    print('  $packageLockJson');
    print('".npmrc" (${npmrc.isNotEmpty ? 'valid' : 'invalid'}):');
    print('  $npmrc');
    print('-- DESCRIPTION END --');
  }
}

/// this method will always return a Directory instance, where:
/// * with no customPath: use the current working directory.
/// * customPath as relative: use relative to the current working directory.
/// * customPath as absolute: use path as-is.
/// ! this does not verify that the directory exists, you must do that yourself.
Future<Directory> getWorkingDirectory(String? customPath) async {
  print('[debug] about to calculate a working directory...');
  if (customPath != null) {
    return Directory(path.normalize(path.absolute(customPath)));
  }
  return Directory.current;
}

Future<Map<String, dynamic>> parseProjectPackageJson(Directory pwd) async {
  // print('[debug] about to try read "package.json"..');
  File packageJsonFile = File(path.join(pwd.path, "package.json"));
  FileStat fileStats = await packageJsonFile.stat();
  // print('[debug] file stats: (${fileStats.type}) $fileStats');
  if (fileStats.type == FileSystemEntityType.notFound) {
    throw FileSystemException('"package.json" not found', packageJsonFile.path);
  }
  String packageJsonString = await packageJsonFile.readAsString();
  return jsonDecode(packageJsonString);
}

Future<Map<String, dynamic>> parseProjectPackageLockJson(Directory pwd) async {
  // print('[debug] about to try read "package-lock.json"..');
  File packageLockJsonFile = File(path.join(pwd.path, "package-lock.json"));
  FileStat fileStats = await packageLockJsonFile.stat();
  // print('[debug] file stats: (${fileStats.type}) $fileStats');
  if (fileStats.type == FileSystemEntityType.notFound) {
    print('[warn] "package-lock.json" not found (${packageLockJsonFile.path})');
    return {};
  }
  String packageJsonString = await packageLockJsonFile.readAsString();
  return jsonDecode(packageJsonString);
}

Future<Map<String, dynamic>> parseProjectNpmrc(Directory pwd) async {
  // print('[debug] about to try read ".npmrc"..');
  File npmrcFile = File(path.join(pwd.path, ".npmrc"));
  FileStat fileStats = await npmrcFile.stat();
  // print('[debug] file stats: (${fileStats.type}) $fileStats');
  if (fileStats.type == FileSystemEntityType.notFound) {
    print('[warn] ".npmrc" not found (${npmrcFile.path})');
    return {};
  }
  List<String> npmrcLines = await npmrcFile.readAsLines();
  return Map.fromEntries(npmrcLines.map((item) {
    List<String> pair = item.split("=");
    return MapEntry(pair[0], pair[1]);
  }));
}
