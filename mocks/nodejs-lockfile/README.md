# NodeJS Project

this is a NodeJS project, and it has a `package-lock.json` file, but it does not contain any `.npmrc` file..

this CLI tool should:

* pin versions according to `package-lock.json` values.
* create a RC file with `save-exact=true`.
