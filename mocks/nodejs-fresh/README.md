# NodeJS Project

this is a NodeJS project, but it does not contain any `.npmrc` file..

this CLI tool should:

* pin versions according to `package.json` value, without applying SemVer rules.
  * _note: this trims `^1.2.3` to `1.2.3`, even if anything newer is available._
* create a RC file with `save-exact=true`.
