# NodeJS Project

this is a NodeJS project, and it has a `package-lock.json` file and a `.npmrc` file, and everything is already pinned..

this CLI tool should:

* ensure versions are pinned according to `package-lock.json` values.
* ensure the RC file contains `save-exact=true`, setting it if found otherwise.
