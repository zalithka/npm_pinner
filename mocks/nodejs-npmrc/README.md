# NodeJS Project

this is a NodeJS project, and it does contain a valid `.npmrc` file..

this CLI tool should:

* pin versions according to `package-lock.json` values.
* ensure the RC file contains `save-exact=true`, setting it if found otherwise.
