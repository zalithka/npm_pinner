# NPM Dependencies Pinner

this exists for one simple reason: I find it monotonous to go through NodeJS projects, pinning dependencies to fixed versions, and ensuring that new ones are locked down with `save-exact` by default.

so, this just does it for me.. :)
